// Copyright 2023 amsl

#include "ee478_lpp.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Local path planner node for EE478");
    EE478_DWAPlanner planner;
    planner.process();
    return 0;
}

// Copyright 2024 URL
// Minho Oh

#include <algorithm>
#include <string>
#include <utility>
#include <vector>

#include "ee478_lpp.h"

EE478_DWAPlanner::EE478_DWAPlanner(void)
    : local_nh_("~"), goal_subscribed_(false), 
      odom_updated_(false), scan_updated_(false), has_reached_(false),
      use_speed_cost_(false), odom_not_subscribe_count_(0), scan_not_subscribe_count_(0)
{
    local_nh_.param<std::string>("ROBOT_FRAME", robot_frame_, {"base_link"});
    local_nh_.param<double>("HZ", hz_, {20});
    local_nh_.param<double>("TARGET_VELOCITY", target_velocity_, {0.55});
    local_nh_.param<double>("MAX_VELOCITY", max_velocity_, {1.0});
    local_nh_.param<double>("MIN_VELOCITY", min_velocity_, {0.0});
    local_nh_.param<double>("MAX_YAWRATE", max_yawrate_, {1.0});
    local_nh_.param<double>("MIN_YAWRATE", min_yawrate_, {0.05});
    local_nh_.param<double>("MAX_IN_PLACE_YAWRATE", max_in_place_yawrate_, {0.6});
    local_nh_.param<double>("MIN_IN_PLACE_YAWRATE", min_in_place_yawrate_, {0.3});
    local_nh_.param<double>("MAX_ACCELERATION", max_acceleration_, {0.5});
    local_nh_.param<double>("MAX_DECELERATION", max_deceleration_, {1.0});
    local_nh_.param<double>("MAX_D_YAWRATE", max_d_yawrate_, {3.2});
    local_nh_.param<double>("ANGLE_RESOLUTION", angle_resolution_, {0.087});
    local_nh_.param<double>("PREDICT_TIME", predict_time_, {3.0});
    local_nh_.param<double>("DT", dt_, {0.1});
    local_nh_.param<double>("SLEEP_TIME_AFTER_FINISH", sleep_time_after_finish_, {0.5});
    local_nh_.param<double>("OBSTACLE_COST_GAIN", obs_cost_gain_, {1.0});
    local_nh_.param<double>("TO_GOAL_COST_GAIN", to_goal_cost_gain_, {1.0});
    local_nh_.param<double>("SPEED_COST_GAIN", speed_cost_gain_, {0.1});
    local_nh_.param<double>("GOAL_THRESHOLD", dist_to_goal_th_, {0.3});
    local_nh_.param<double>("TURN_DIRECTION_THRESHOLD", turn_direction_th_, {1.0});
    local_nh_.param<double>("ANGLE_TO_GOAL_TH", angle_to_goal_th_, {M_PI});
    local_nh_.param<double>("SIM_DIRECTION", sim_direction_, {M_PI / 2.0});
    local_nh_.param<double>("SLOW_VELOCITY_TH", slow_velocity_th_, {0.1});
    local_nh_.param<double>("OBS_RANGE", obs_range_, {2.5});
    local_nh_.param<bool>("USE_SPEED_COST", use_speed_cost_, {false});
    local_nh_.param<bool>("USE_FOOTPRINT", use_footprint_, {false});
    local_nh_.param<int>("SUBSCRIBE_COUNT_TH", subscribe_count_th_, {3});
    local_nh_.param<int>("VELOCITY_SAMPLES", velocity_samples_, {3});
    local_nh_.param<int>("YAWRATE_SAMPLES", yawrate_samples_, {20});

    ROS_INFO("=== DWA Planner ===");
    ROS_INFO_STREAM("ROBOT_FRAME: " << robot_frame_);
    ROS_INFO_STREAM("HZ: " << hz_);
    ROS_INFO_STREAM("TARGET_VELOCITY: " << target_velocity_);
    ROS_INFO_STREAM("MAX_VELOCITY: " << max_velocity_);
    ROS_INFO_STREAM("MIN_VELOCITY: " << min_velocity_);
    ROS_INFO_STREAM("MAX_YAWRATE: " << max_yawrate_);
    ROS_INFO_STREAM("MIN_YAWRATE: " << min_yawrate_);
    ROS_INFO_STREAM("MAX_IN_PLACE_YAWRATE: " << max_in_place_yawrate_);
    ROS_INFO_STREAM("MIN_IN_PLACE_YAWRATE: " << min_in_place_yawrate_);
    ROS_INFO_STREAM("MAX_ACCELERATION: " << max_acceleration_);
    ROS_INFO_STREAM("MAX_DECELERATION: " << max_deceleration_);
    ROS_INFO_STREAM("MAX_D_YAWRATE: " << max_d_yawrate_);
    ROS_INFO_STREAM("ANGLE_RESOLUTION: " << angle_resolution_);
    ROS_INFO_STREAM("PREDICT_TIME: " << predict_time_);
    ROS_INFO_STREAM("DT: " << dt_);
    ROS_INFO_STREAM("SLEEP_TIME_AFTER_FINISH: " << sleep_time_after_finish_);
    ROS_INFO_STREAM("OBSTACLE_COST_GAIN: " << obs_cost_gain_);
    ROS_INFO_STREAM("TO_GOAL_COST_GAIN: " << to_goal_cost_gain_);
    ROS_INFO_STREAM("SPEED_COST_GAIN: " << speed_cost_gain_);
    ROS_INFO_STREAM("GOAL_THRESHOLD: " << dist_to_goal_th_);
    ROS_INFO_STREAM("TURN_DIRECTION_THRESHOLD: " << turn_direction_th_);
    ROS_INFO_STREAM("ANGLE_TO_GOAL_TH: " << angle_to_goal_th_);
    ROS_INFO_STREAM("SIM_DIRECTION: " << sim_direction_);
    ROS_INFO_STREAM("SLOW_VELOCITY_TH: " << slow_velocity_th_);
    ROS_INFO_STREAM("OBS_RANGE: " << obs_range_);
    ROS_INFO_STREAM("USE_SPEED_COST: " << use_speed_cost_);
    ROS_INFO_STREAM("USE_FOOTPRINT: " << use_footprint_);
    ROS_INFO_STREAM("SUBSCRIBE_COUNT_TH: " << subscribe_count_th_);
    ROS_INFO_STREAM("VELOCITY_SAMPLES: " << velocity_samples_);
    ROS_INFO_STREAM("YAWRATE_SAMPLES: " << yawrate_samples_);

    velocity_pub_               = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    candidate_trajectories_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("/lpp/candidate_trajectories", 1);
    selected_trajectory_pub_    = nh_.advertise<visualization_msgs::Marker>("/lpp/selected_trajectory", 1);
    predict_footprints_pub_     = nh_.advertise<visualization_msgs::MarkerArray>("/lpp/predict_footprints", 1);
    cur_goal_pub_               = nh_.advertise<visualization_msgs::MarkerArray>("/lpp/cur_local_goal", 1);
    finish_flag_pub_            = nh_.advertise<std_msgs::Bool>("/lpp/finish_flag", 1);

    // world2base_sub_ = nh_.subscribe("/world2base", 1, &EE478_DWAPlanner::world2base_cb, this);
    goal_sub_       = nh_.subscribe("/sub_goal", 1, &EE478_DWAPlanner::goal_cb, this);
    odom2base_sub_  = nh_.subscribe("/odom", 1, &EE478_DWAPlanner::odom_cb, this);
    scan_sub_       = nh_.subscribe("/scan", 1, &EE478_DWAPlanner::scan_cb, this);

    scan_updated_ = false;

    // set footprint for jackal
    float jackal_length = 0.6;
    float jackal_width = 0.5;
    footprint_.header.frame_id = robot_frame_;
    footprint_.polygon.points.resize(4);
    footprint_.polygon.points[0].x = jackal_length/2;
    footprint_.polygon.points[0].y = jackal_width/2;
    footprint_.polygon.points[1].x = jackal_length/2;
    footprint_.polygon.points[1].y = -jackal_width/2;
    footprint_.polygon.points[2].x = -jackal_length/2;
    footprint_.polygon.points[2].y = -jackal_width/2;
    footprint_.polygon.points[3].x = -jackal_length/2;
    footprint_.polygon.points[3].y = jackal_width/2;

    // set scenario


    return;
}

EE478_DWAPlanner::State::State(void) : x_(0.0), y_(0.0), yaw_(0.0), velocity_(0.0), yawrate_(0.0) {}

EE478_DWAPlanner::State::State(const double x, const double y, const double yaw, const double velocity, const double yawrate)
    : x_(x), y_(y), yaw_(yaw), velocity_(velocity), yawrate_(yawrate)
{
}

EE478_DWAPlanner::Window::Window(void) : min_velocity_(0.0), max_velocity_(0.0), min_yawrate_(0.0), max_yawrate_(0.0) {}

EE478_DWAPlanner::Window::Window(const double min_v, const double max_v, const double min_y, const double max_y)
    : min_velocity_(min_v), max_velocity_(max_v), min_yawrate_(min_y), max_yawrate_(max_y)
{
}

EE478_DWAPlanner::Cost::Cost(void) : obs_cost_(0.0), to_goal_cost_(0.0), speed_cost_(0.0), total_cost_(0.0)
{
}

EE478_DWAPlanner::Cost::Cost(
    const float obs_cost, const float to_goal_cost, const float speed_cost, 
    const float total_cost)
    : obs_cost_(obs_cost), to_goal_cost_(to_goal_cost), speed_cost_(speed_cost), 
      total_cost_(total_cost)
{
}

void EE478_DWAPlanner::Cost::show(void)
{
    ROS_INFO_STREAM("Cost: " << total_cost_);
    ROS_INFO_STREAM("\tObs cost: " << obs_cost_);
    ROS_INFO_STREAM("\tGoal cost: " << to_goal_cost_);
    ROS_INFO_STREAM("\tSpeed cost: " << speed_cost_);
}

void EE478_DWAPlanner::Cost::calc_total_cost(void) { total_cost_ = obs_cost_ + to_goal_cost_ + speed_cost_; }

void EE478_DWAPlanner::scan_cb(const sensor_msgs::LaserScanConstPtr &msg){
    scan_to_obs(*msg);
    scan_not_subscribe_count_ = 0;
    scan_updated_ = true;
}

void EE478_DWAPlanner::goal_cb(const geometry_msgs::PoseStampedConstPtr &msg){
    global_goal_ = *msg;
    goal_subscribed_ = true;

}

// void EE478_DWAPlanner::world2base_cb(const nav_msgs::OdometryConstPtr &msg){
//     world2base_odom_ = *msg;
// }

void EE478_DWAPlanner::odom_cb(const nav_msgs::OdometryConstPtr &msg){
    // std::cout << "odom_cb" << std::endl;

    // nav_msgs::Odometry gt_odom = world2base_odom_;
    // nav_msgs::Odometry cur_odom = *msg;
    // odom_frame_ = cur_odom.header.frame_id;
    // tf::Quaternion q_gt;
    // q_gt.setX(gt_odom.pose.pose.orientation.x);
    // q_gt.setY(gt_odom.pose.pose.orientation.y);
    // q_gt.setZ(gt_odom.pose.pose.orientation.z);
    // q_gt.setW(gt_odom.pose.pose.orientation.w);
    // tf::Transform tf_gt;
    // tf_gt.setOrigin(tf::Vector3(gt_odom.pose.pose.position.x, gt_odom.pose.pose.position.y, gt_odom.pose.pose.position.z));
    // tf_gt.setRotation(q_gt);
    

    // tf::Quaternion q_cur;
    // q_cur.setX(cur_odom.pose.pose.orientation.x);
    // q_cur.setY(cur_odom.pose.pose.orientation.y);
    // q_cur.setZ(cur_odom.pose.pose.orientation.z);
    // q_cur.setW(cur_odom.pose.pose.orientation.w);
    // tf::Transform tf_cur;
    // tf_cur.setOrigin(tf::Vector3(cur_odom.pose.pose.position.x, cur_odom.pose.pose.position.y, cur_odom.pose.pose.position.z));
    // tf_cur.setRotation(q_cur);

    // tf::Transform tf_g2c = tf_gt * tf_cur.inverse();
    // tf::Quaternion q_g2c = tf_g2c.getRotation();
    // tf::Vector3 v_g2c = tf_g2c.getOrigin();

    // static tf::TransformBroadcaster br_tf_g2c;
    // br_tf_g2c.sendTransform(tf::StampedTransform(tf_g2c, msg->header.stamp, gt_odom.header.frame_id, cur_odom.header.frame_id));
    
    current_cmd_vel_ = msg->twist.twist;
    odom_not_subscribe_count_ = 0;
    odom_updated_ = true;
    if (goal_subscribed_){
        try
        {
            listener_.transformPose(robot_frame_, ros::Time(0), global_goal_, global_goal_.header.frame_id, local_goal_);
        }
        catch (tf::TransformException ex)
        {
            ROS_ERROR("%s", ex.what());
        }
    }
    return;
}



std::vector<EE478_DWAPlanner::State>
EE478_DWAPlanner::dwa_planning(const Eigen::Vector3d &goal, std::vector<std::pair<std::vector<State>, bool>> &trajectories)
{
    Cost min_cost(0.0, 0.0, 0.0, 1e6);
    const Window dynamic_window = calc_dynamic_window();
    const size_t trajectory_size = predict_time_ / dt_;
    std::vector<State> best_traj;
    best_traj.resize(trajectory_size);
    std::vector<Cost> costs;
    const size_t costs_size = velocity_samples_ * (yawrate_samples_ + 1);
    costs.reserve(costs_size);

    const double velocity_resolution =
        std::max((dynamic_window.max_velocity_ - dynamic_window.min_velocity_) / (velocity_samples_ - 1), DBL_EPSILON);
    const double yawrate_resolution =
        std::max((dynamic_window.max_yawrate_ - dynamic_window.min_yawrate_) / (yawrate_samples_ - 1), DBL_EPSILON);

    int available_traj_count = 0;
    for (int i = 0; i < velocity_samples_; i++)
    {
        const double v = dynamic_window.min_velocity_ + velocity_resolution * i;
        for (int j = 0; j < yawrate_samples_; j++)
        {
            std::pair<std::vector<State>, bool> traj;
            double y = dynamic_window.min_yawrate_ + yawrate_resolution * j;
            if (v < slow_velocity_th_)
                y = y > 0 ? std::max(y, min_yawrate_) : std::min(y, -min_yawrate_);
            traj.first = generate_trajectory(v, y);

            const Cost cost = evaluate_trajectory(traj.first, goal);
            costs.push_back(cost);
            if (cost.obs_cost_ == 1e6)
            {
                traj.second = false;
            }
            else
            {
                traj.second = true;
                available_traj_count++;
            }
            trajectories.push_back(traj);
        }

        if (dynamic_window.min_yawrate_ < 0.0 && 0.0 < dynamic_window.max_yawrate_)
        {
            std::pair<std::vector<State>, bool> traj;
            traj.first = generate_trajectory(v, 0.0);
            const Cost cost = evaluate_trajectory(traj.first, goal);
            costs.push_back(cost);
            if (cost.obs_cost_ == 1e6)
            {
                traj.second = false;
            }
            else
            {
                traj.second = true;
                available_traj_count++;
            }
            trajectories.push_back(traj);
        }
    }

    if (available_traj_count == 0)
    {
        ROS_ERROR_THROTTLE(1.0, "No available trajectory");
        best_traj = generate_trajectory(0.0, 0.0);
    }
    else
    {
        normalize_costs(costs);
        for (int i = 0; i < costs.size(); i++)
        {
            if (costs[i].obs_cost_ != 1e6)
            {
                costs[i].to_goal_cost_ *= to_goal_cost_gain_;
                costs[i].obs_cost_ *= obs_cost_gain_;
                costs[i].speed_cost_ *= speed_cost_gain_;
                costs[i].calc_total_cost();
                if (costs[i].total_cost_ < min_cost.total_cost_)
                {
                    min_cost = costs[i];
                    best_traj = trajectories[i].first;
                }
            }
        }
    }

    ROS_INFO_STREAM("===");
    ROS_INFO_STREAM("(v, y) = (" << best_traj.front().velocity_ << ", " << best_traj.front().yawrate_ << ")");
    min_cost.show();
    ROS_INFO_STREAM("num of trajectories available: " << available_traj_count << " of " << trajectories.size());
    ROS_INFO_STREAM(" ");

    return best_traj;
}

void EE478_DWAPlanner::normalize_costs(std::vector<EE478_DWAPlanner::Cost> &costs)
{
    Cost min_cost(1e6, 1e6, 1e6, 1e6), max_cost;

    for (const auto &cost : costs)
    {
        if (cost.obs_cost_ != 1e6)
        {
            min_cost.obs_cost_ = std::min(min_cost.obs_cost_, cost.obs_cost_);
            max_cost.obs_cost_ = std::max(max_cost.obs_cost_, cost.obs_cost_);
            min_cost.to_goal_cost_ = std::min(min_cost.to_goal_cost_, cost.to_goal_cost_);
            max_cost.to_goal_cost_ = std::max(max_cost.to_goal_cost_, cost.to_goal_cost_);
            if (use_speed_cost_)
            {
                min_cost.speed_cost_ = std::min(min_cost.speed_cost_, cost.speed_cost_);
                max_cost.speed_cost_ = std::max(max_cost.speed_cost_, cost.speed_cost_);
            }
        }
    }

    for (auto &cost : costs)
    {
        if (cost.obs_cost_ != 1e6)
        {
            cost.obs_cost_ =
                (cost.obs_cost_ - min_cost.obs_cost_) / (max_cost.obs_cost_ - min_cost.obs_cost_ + DBL_EPSILON);
            cost.to_goal_cost_ = (cost.to_goal_cost_ - min_cost.to_goal_cost_) /
                                 (max_cost.to_goal_cost_ - min_cost.to_goal_cost_ + DBL_EPSILON);
            if (use_speed_cost_)
                cost.speed_cost_ = (cost.speed_cost_ - min_cost.speed_cost_) /
                                   (max_cost.speed_cost_ - min_cost.speed_cost_ + DBL_EPSILON);
        }
    }
}

void EE478_DWAPlanner::process(void)
{
    ros::Rate loop_rate(hz_);
    bool init = true;
    while (ros::ok())
    {
        geometry_msgs::Twist cmd_vel;
        if (can_move())
            cmd_vel = calc_cmd_vel();

        velocity_pub_.publish(cmd_vel);
        finish_flag_pub_.publish(has_finished_);

        if (has_reached_){
            // std::cout << "REACHED goal: " << goal_list_[0].first.pose.position.x << ", " << goal_list_[0].first.pose.position.y << std::endl;
            // goal_list_.erase(goal_list_.begin());
            ros::Duration(sleep_time_after_finish_).sleep();
            has_reached_ = false;
        }

        scan_updated_ = false;
        odom_updated_ = false;
        has_finished_.data = false;

        ros::spinOnce();
        loop_rate.sleep();
    }
}

bool EE478_DWAPlanner::can_move(void)
{

    if (!goal_subscribed_)
        ROS_WARN_THROTTLE(1.0, "[LPP] Local goal has not been updated");
    if (subscribe_count_th_ < odom_not_subscribe_count_)
        ROS_WARN_THROTTLE(1.0, "[LPP] Odom has not been updated");
    if (subscribe_count_th_ < scan_not_subscribe_count_)
        ROS_WARN_THROTTLE(1.0, "[LPP] Scan has not been updated");

    if (!odom_updated_)
        odom_not_subscribe_count_++;
    if (!scan_updated_)
        scan_not_subscribe_count_++;

    if (
        goal_subscribed_ && 
        odom_not_subscribe_count_ <= subscribe_count_th_ && 
        scan_not_subscribe_count_ <= subscribe_count_th_)
        return true;
    else
        return false;
}

geometry_msgs::Twist EE478_DWAPlanner::calc_cmd_vel(void)
{
    geometry_msgs::Twist cmd_vel;
    std::pair<std::vector<State>, bool> best_traj;
    std::vector<std::pair<std::vector<State>, bool>> trajectories;
    const size_t trajectories_size = velocity_samples_ * (yawrate_samples_ + 1);
    trajectories.reserve(trajectories_size);

    const Eigen::Vector3d goal(local_goal_.pose.position.x, local_goal_.pose.position.y, tf::getYaw(local_goal_.pose.orientation));
    const double angle_to_goal = atan2(goal.y(), goal.x());
    if (M_PI / 4.0 < fabs(angle_to_goal))
        use_speed_cost_ = true;

    if (dist_to_goal_th_ < goal.segment(0, 2).norm() && !has_reached_)
    {
        if (can_adjust_robot_direction(goal))
        {
            cmd_vel.angular.z = angle_to_goal > 0 ? std::min(angle_to_goal, max_in_place_yawrate_)
                                                  : std::max(angle_to_goal, -max_in_place_yawrate_);
            cmd_vel.angular.z = cmd_vel.angular.z > 0 ? std::max(cmd_vel.angular.z, min_in_place_yawrate_)
                                                      : std::min(cmd_vel.angular.z, -min_in_place_yawrate_);
            best_traj.first = generate_trajectory(cmd_vel.angular.z, goal);
            trajectories.push_back(best_traj);
        }
        else
        {
            best_traj.first = dwa_planning(goal, trajectories);
            cmd_vel.linear.x = best_traj.first.front().velocity_;
            cmd_vel.angular.z = best_traj.first.front().yawrate_;
        }
    }
    else
    {
        has_reached_ = true;
        if (turn_direction_th_ < fabs(goal[2]))
        {
            cmd_vel.angular.z =
                goal[2] > 0 ? std::min(goal[2], max_in_place_yawrate_) : std::max(goal[2], -max_in_place_yawrate_);
            cmd_vel.angular.z = cmd_vel.angular.z > 0 ? std::max(cmd_vel.angular.z, min_in_place_yawrate_)
                                                      : std::min(cmd_vel.angular.z, -min_in_place_yawrate_);
        }
        else
        {
            has_finished_.data = true;
            has_reached_ = false;
        }
        best_traj.first = generate_trajectory(cmd_vel.linear.x, cmd_vel.angular.z);
        trajectories.push_back(best_traj);
    }

    visualize_trajectory(best_traj.first, 1, 0, 0, selected_trajectory_pub_);
    visualize_trajectories(trajectories, 0, 1, 0, candidate_trajectories_pub_);
    visualize_footprints(best_traj.first, 0, 0, 1, predict_footprints_pub_);
    visualize_goal(global_goal_, cur_goal_pub_);

    use_speed_cost_ = false;

    return cmd_vel;
}

bool EE478_DWAPlanner::can_adjust_robot_direction(const Eigen::Vector3d &goal)
{
    const double angle_to_goal = atan2(goal.y(), goal.x());
    if (fabs(angle_to_goal) < angle_to_goal_th_)
        return false;

    const double yawrate = std::min(std::max(angle_to_goal, -max_in_place_yawrate_), max_in_place_yawrate_);
    std::vector<State> traj = generate_trajectory(yawrate, goal);

    if (!check_collision(traj))
        return true;
    else
        return false;
}

bool EE478_DWAPlanner::check_collision(const std::vector<State> &traj)
{
    for (const auto &state : traj)
    {
        for (const auto &obs : obs_list_.poses)
        {
            const geometry_msgs::PolygonStamped footprint = transform_footprint(state);
            if (is_inside_of_robot(obs.position, footprint, state))
                return true;
        }
    }

    return false;
}

EE478_DWAPlanner::Window EE478_DWAPlanner::calc_dynamic_window(void)
{
    Window window(min_velocity_, max_velocity_, -max_yawrate_, max_yawrate_);
    window.min_velocity_ = std::max((current_cmd_vel_.linear.x - max_deceleration_ * dt_), min_velocity_);
    window.max_velocity_ = std::min((current_cmd_vel_.linear.x + max_acceleration_ * dt_), target_velocity_);
    window.min_yawrate_ = std::max((current_cmd_vel_.angular.z - max_d_yawrate_ * dt_), -max_yawrate_);
    window.max_yawrate_ = std::min((current_cmd_vel_.angular.z + max_d_yawrate_ * dt_), max_yawrate_);
    return window;
}

float EE478_DWAPlanner::calc_to_goal_cost(const std::vector<State> &traj, const Eigen::Vector3d &goal)
{
    Eigen::Vector3d last_position(traj.back().x_, traj.back().y_, traj.back().yaw_);
    return (last_position.segment(0, 2) - goal.segment(0, 2)).norm();
}

float EE478_DWAPlanner::calc_obs_cost(const std::vector<State> &traj)
{
    float min_dist = obs_range_;
    for (const auto &state : traj)
    {
        for (const auto &obs : obs_list_.poses)
        {
            float dist;
            if (use_footprint_)
                dist = calc_dist_from_robot(obs.position, state);
            else
                dist = hypot((state.x_ - obs.position.x), (state.y_ - obs.position.y));

            if (dist < DBL_EPSILON)
                return 1e6;
            min_dist = std::min(min_dist, dist);
        }
    }
    return obs_range_ - min_dist;
}

float EE478_DWAPlanner::calc_speed_cost(const std::vector<State> &traj)
{
    if (!use_speed_cost_)
        return 0.0;
    const Window dynamic_window = calc_dynamic_window();
    return dynamic_window.max_velocity_ - traj.front().velocity_;
}

std::vector<EE478_DWAPlanner::State> EE478_DWAPlanner::generate_trajectory(const double velocity, const double yawrate)
{
    const size_t trajectory_size = predict_time_ / dt_;
    std::vector<State> trajectory;
    trajectory.resize(trajectory_size);
    State state;
    for (int i = 0; i < trajectory_size; i++)
    {
        motion(state, velocity, yawrate);
        trajectory[i] = state;
    }
    return trajectory;
}

std::vector<EE478_DWAPlanner::State> EE478_DWAPlanner::generate_trajectory(const double yawrate, const Eigen::Vector3d &goal)
{
    const double target_direction = atan2(goal.y(), goal.x()) > 0 ? sim_direction_ : -sim_direction_;
    const double predict_time = target_direction / (yawrate + DBL_EPSILON);
    const size_t trajectory_size = predict_time / dt_;
    std::vector<State> trajectory;
    trajectory.resize(trajectory_size);
    State state;
    for (int i = 0; i < trajectory_size; i++)
    {
        motion(state, 0.0, yawrate);
        trajectory[i] = state;
    }
    return trajectory;
}

EE478_DWAPlanner::Cost EE478_DWAPlanner::evaluate_trajectory(const std::vector<State> &trajectory, const Eigen::Vector3d &goal)
{
    Cost cost;
    cost.to_goal_cost_ = calc_to_goal_cost(trajectory, goal);
    cost.obs_cost_ = calc_obs_cost(trajectory);
    cost.speed_cost_ = calc_speed_cost(trajectory);
    cost.calc_total_cost();
    return cost;
}

geometry_msgs::Point EE478_DWAPlanner::calc_intersection(
    const geometry_msgs::Point &obstacle, const State &state, geometry_msgs::PolygonStamped footprint)
{
    for (int i = 0; i < footprint.polygon.points.size(); i++)
    {
        const Eigen::Vector3d vector_A(obstacle.x, obstacle.y, 0.0);
        const Eigen::Vector3d vector_B(state.x_, state.y_, 0.0);
        const Eigen::Vector3d vector_C(footprint.polygon.points[i].x, footprint.polygon.points[i].y, 0.0);
        Eigen::Vector3d vector_D(0.0, 0.0, 0.0);
        if (i != footprint.polygon.points.size() - 1)
            vector_D << footprint.polygon.points[i + 1].x, footprint.polygon.points[i + 1].y, 0.0;
        else
            vector_D << footprint.polygon.points[0].x, footprint.polygon.points[0].y, 0.0;

        const double deno = (vector_B - vector_A).cross(vector_D - vector_C).z();
        const double s = (vector_C - vector_A).cross(vector_D - vector_C).z() / deno;
        const double t = (vector_B - vector_A).cross(vector_A - vector_C).z() / deno;

        geometry_msgs::Point point;
        point.x = vector_A.x() + s * (vector_B - vector_A).x();
        point.y = vector_A.y() + s * (vector_B - vector_A).y();

        // cross
        if (!(s < 0.0 || 1.0 < s || t < 0.0 || 1.0 < t))
            return point;
    }

    geometry_msgs::Point point;
    point.x = 1e6;
    point.y = 1e6;
    return point;
}

float EE478_DWAPlanner::calc_dist_from_robot(const geometry_msgs::Point &obstacle, const State &state)
{
    const geometry_msgs::PolygonStamped footprint = transform_footprint(state);
    if (is_inside_of_robot(obstacle, footprint, state))
    {
        return 0.0;
    }
    else
    {
        geometry_msgs::Point intersection = calc_intersection(obstacle, state, footprint);
        return hypot((obstacle.x - intersection.x), (obstacle.y - intersection.y));
    }
}

geometry_msgs::PolygonStamped EE478_DWAPlanner::transform_footprint(const State &target_pose)
{
    geometry_msgs::PolygonStamped footprint = footprint_;
    footprint.header.stamp = ros::Time::now();

    for (auto &point : footprint.polygon.points)
    {
        Eigen::VectorXf point_in(2);
        point_in << point.x, point.y;
        Eigen::Matrix2f rot;
        rot = Eigen::Rotation2Df(target_pose.yaw_);
        const Eigen::VectorXf point_out = rot * point_in;

        point.x = point_out.x() + target_pose.x_;
        point.y = point_out.y() + target_pose.y_;
    }

    return footprint;
}

bool EE478_DWAPlanner::is_inside_of_robot(
    const geometry_msgs::Point &obstacle, const geometry_msgs::PolygonStamped &footprint, const State &state)
{
    geometry_msgs::Point32 state_point;
    state_point.x = state.x_;
    state_point.y = state.y_;

    for (int i = 0; i < footprint.polygon.points.size(); i++)
    {
        geometry_msgs::Polygon triangle;
        triangle.points.push_back(state_point);
        triangle.points.push_back(footprint.polygon.points[i]);

        if (i != footprint.polygon.points.size() - 1)
            triangle.points.push_back(footprint.polygon.points[i + 1]);
        else
            triangle.points.push_back(footprint.polygon.points[0]);

        if (is_inside_of_triangle(obstacle, triangle))
            return true;
    }

    return false;
}

bool EE478_DWAPlanner::is_inside_of_triangle(const geometry_msgs::Point &target_point, const geometry_msgs::Polygon &triangle)
{
    if (triangle.points.size() != 3)
    {
        ROS_ERROR("[LPP] Not triangle");
        exit(1);
    }

    const Eigen::Vector3d vector_A(triangle.points[0].x, triangle.points[0].y, 0.0);
    const Eigen::Vector3d vector_B(triangle.points[1].x, triangle.points[1].y, 0.0);
    const Eigen::Vector3d vector_C(triangle.points[2].x, triangle.points[2].y, 0.0);
    const Eigen::Vector3d vector_P(target_point.x, target_point.y, 0.0);

    const Eigen::Vector3d vector_AB = vector_B - vector_A;
    const Eigen::Vector3d vector_BP = vector_P - vector_B;
    const Eigen::Vector3d cross1 = vector_AB.cross(vector_BP);

    const Eigen::Vector3d vector_BC = vector_C - vector_B;
    const Eigen::Vector3d vector_CP = vector_P - vector_C;
    const Eigen::Vector3d cross2 = vector_BC.cross(vector_CP);

    const Eigen::Vector3d vector_CA = vector_A - vector_C;
    const Eigen::Vector3d vector_AP = vector_P - vector_A;
    const Eigen::Vector3d cross3 = vector_CA.cross(vector_AP);

    if ((0 < cross1.z() && 0 < cross2.z() && 0 < cross3.z()) || (cross1.z() < 0 && cross2.z() < 0 && cross3.z() < 0))
        return true;
    else
        return false;
}

void EE478_DWAPlanner::motion(State &state, const double velocity, const double yawrate)
{
    state.yaw_ += yawrate * dt_;
    state.x_ += velocity * std::cos(state.yaw_) * dt_;
    state.y_ += velocity * std::sin(state.yaw_) * dt_;
    state.velocity_ = velocity;
    state.yawrate_ = yawrate;
}

void EE478_DWAPlanner::scan_to_obs(const sensor_msgs::LaserScan &scan)
{
    obs_list_.poses.clear();
    float angle = scan.angle_min;
    for (auto r : scan.ranges)
    {
        geometry_msgs::Pose pose;
        pose.position.x = r * cos(angle) + 0.12; // 0.12 is the distance from the base_link to the laser
        pose.position.y = r * sin(angle);
        obs_list_.poses.push_back(pose);
        angle += scan.angle_increment;
    }
}

void EE478_DWAPlanner::raycast(const nav_msgs::OccupancyGrid &map)
{
    obs_list_.poses.clear();
    const double max_search_dist = hypot(map.info.origin.position.x, map.info.origin.position.y);
    for (float angle = -M_PI; angle <= M_PI; angle += angle_resolution_)
    {
        for (float dist = 0.0; dist <= max_search_dist; dist += map.info.resolution)
        {
            geometry_msgs::Pose pose;
            pose.position.x = dist * cos(angle);
            pose.position.y = dist * sin(angle);
            const int index_x = floor((pose.position.x - map.info.origin.position.x) / map.info.resolution);
            const int index_y = floor((pose.position.y - map.info.origin.position.y) / map.info.resolution);

            if ((0 <= index_x && index_x < map.info.width) && (0 <= index_y && index_y < map.info.height))
            {
                if (map.data[index_x + index_y * map.info.width] == 100)
                {
                    obs_list_.poses.push_back(pose);
                    break;
                }
            }
        }
    }
}

void EE478_DWAPlanner::visualize_trajectories(
    const std::vector<std::pair<std::vector<State>, bool>> &trajectories, const double r, const double g,
    const double b, const ros::Publisher &pub)
{
    visualization_msgs::MarkerArray v_trajectories;
    for (int count = 0; count < trajectories.size(); count++)
    {
        visualization_msgs::Marker v_trajectory;
        v_trajectory.header.frame_id = robot_frame_;
        v_trajectory.header.stamp = ros::Time::now();
        if (trajectories[count].second)
        {
            v_trajectory.color.r = r;
            v_trajectory.color.g = g;
            v_trajectory.color.b = b;
        }
        else
        {
            v_trajectory.color.r = 0.5;
            v_trajectory.color.g = 0.0;
            v_trajectory.color.b = 0.5;
        }
        v_trajectory.color.a = 0.8;
        v_trajectory.ns = pub.getTopic();
        v_trajectory.type = visualization_msgs::Marker::LINE_STRIP;
        v_trajectory.action = visualization_msgs::Marker::ADD;
        v_trajectory.lifetime = ros::Duration();
        v_trajectory.id = count;
        v_trajectory.scale.x = 0.02;
        geometry_msgs::Pose pose;
        pose.orientation.w = 1;
        v_trajectory.pose = pose;
        geometry_msgs::Point p;
        for (const auto &pose : trajectories[count].first)
        {
            p.x = pose.x_;
            p.y = pose.y_;
            v_trajectory.points.push_back(p);
        }
        v_trajectories.markers.push_back(v_trajectory);
    }
    pub.publish(v_trajectories);
}

void EE478_DWAPlanner::visualize_trajectory(
    const std::vector<State> &trajectory, const double r, const double g, const double b, const ros::Publisher &pub)
{
    visualization_msgs::Marker v_trajectory;
    v_trajectory.header.frame_id = robot_frame_;
    v_trajectory.header.stamp = ros::Time::now();
    v_trajectory.color.r = r;
    v_trajectory.color.g = g;
    v_trajectory.color.b = b;
    v_trajectory.color.a = 0.8;
    v_trajectory.ns = pub.getTopic();
    v_trajectory.type = visualization_msgs::Marker::LINE_STRIP;
    v_trajectory.action = visualization_msgs::Marker::ADD;
    v_trajectory.lifetime = ros::Duration();
    v_trajectory.scale.x = 0.1;
    geometry_msgs::Pose pose;
    pose.orientation.w = 1;
    v_trajectory.pose = pose;
    geometry_msgs::Point p;
    for (const auto &pose : trajectory)
    {
        p.x = pose.x_;
        p.y = pose.y_;
        v_trajectory.points.push_back(p);
    }
    pub.publish(v_trajectory);
}

void EE478_DWAPlanner::visualize_footprints(
    const std::vector<State> &trajectory, const double r, const double g, const double b, const ros::Publisher &pub)
{
    visualization_msgs::MarkerArray v_footprints;
    for (int i = 0; i < trajectory.size(); i++)
    {
        visualization_msgs::Marker v_footprint;
        v_footprint.header.frame_id = robot_frame_;
        v_footprint.header.stamp = ros::Time::now();
        v_footprint.color.r = r;
        v_footprint.color.g = g;
        v_footprint.color.b = b;
        v_footprint.color.a = 0.8;
        v_footprint.ns = pub.getTopic();
        v_footprint.type = visualization_msgs::Marker::LINE_STRIP;
        v_footprint.action = visualization_msgs::Marker::ADD;
        v_footprint.lifetime = ros::Duration();
        v_footprint.id = i;
        v_footprint.scale.x = 0.05;
        geometry_msgs::Pose pose;
        pose.orientation.w = 1;
        v_footprint.pose = pose;
        geometry_msgs::Point p;
        const geometry_msgs::PolygonStamped footprint = transform_footprint(trajectory[i]);
        for (const auto &point : footprint.polygon.points)
        {
            p.x = point.x;
            p.y = point.y;
            v_footprint.points.push_back(p);
        }
        p.x = footprint.polygon.points.front().x;
        p.y = footprint.polygon.points.front().y;
        v_footprint.points.push_back(p);
        v_footprints.markers.push_back(v_footprint);
    }
    pub.publish(v_footprints);
}

void EE478_DWAPlanner::visualize_goal(
    const geometry_msgs::PoseStamped &goal, const ros::Publisher &pub)
{
    visualization_msgs::MarkerArray v_goals;
    visualization_msgs::Marker v_goal;
    v_goal.header.frame_id = goal.header.frame_id;
    v_goal.header.stamp = ros::Time::now();
    v_goal.color.g = 1.0;
    v_goal.color.r = 0.0;
    v_goal.color.b = 1.0;

    v_goal.color.a = 1.0;
    v_goal.ns = pub.getTopic();
    v_goal.type = visualization_msgs::Marker::ARROW;
    v_goal.action = visualization_msgs::Marker::ADD;
    v_goal.lifetime = ros::Duration();
    v_goal.id = 1;

    v_goal.pose.position.x = goal.pose.position.x;
    v_goal.pose.position.y = goal.pose.position.y;
    v_goal.pose.position.z = 0.0;
    v_goal.pose.orientation = goal.pose.orientation;
    v_goal.scale.x = 0.8;
    v_goal.scale.y = 0.4;
    v_goal.scale.z = 0.4;        
    v_goals.markers.push_back(v_goal);
    pub.publish(v_goals);
}


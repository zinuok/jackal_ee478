# jackal_ee478
for EE478 class mini project

# Install Dependencies
```
# Install Jackal dependencies for Gazebo
sudo apt install -y ros-$ROS_DISTRO-jackal-cartographer-navigation ros-$ROS_DISTRO-jackal-control ros-$ROS_DISTRO-jackal-description ros-$ROS_DISTRO-jackal-desktop ros-$ROS_DISTRO-jackal-gazebo ros-$ROS_DISTRO-jackal-msgs ros-$ROS_DISTRO-jackal-navigation ros-$ROS_DISTRO-jackal-navigation ros-$ROS_DISTRO-jackal-tutorials ros-$ROS_DISTRO-jackal-simulator ros-$ROS_DISTRO-jackal-viz
```

```
# Install Camera sensor dependencies for Gazebo
sudo apt install -y ros-$ROS_DISTRO-realsense2-camera ros-$ROS_DISTRO-realsense2-description ros-$ROS_DISTRO-gazebo-plugins
```

```
# Set-up to use EE487 Gazebo models and worlds 
cd (workspace)/src/jackal_ee478
      export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/home/zinuok/ros/ee478_ws/src/jackal_ee478/include/gazebo_models_worlds/models
echo "export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$(pwd)/jackal_ee478/include/gazebo_models_worlds/models" >> ~/.bashrc
echo "export GAZEBO_RESOURCE_PATH=$GAZEBO_RESOURCE_PATH:$(pwd)/jackal_ee478/include/gazebo_models_worlds/worlds" >> ~/.bashrc
source ~/.bashrc
```

# Run
## Open the Gazebo World for EE478 Project
```
cd (workspace)
source devel/setup.bash
roslaunch jackal_ee478 jackal_world.launch

```

## Run 2D LiDAR SLAM and Local Planner: Gmapping & DWA algorithm
```
cd (workspace)
source devel/setup.bash
roslaunch ee478_lpp run_lpp.launch
```

## \[Task\] Run YOUR GLOBAL Path Planning
```
cd (workspace)
source devel/setup.bash
roslaunch ee478_gpp run_gpp.launch
```
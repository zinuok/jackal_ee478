
#include "utils.h"

geometry_msgs::PoseStamped goal_pt_;
nav_msgs::Odometry odom_;
nav_msgs::OccupancyGrid occmap_;
bool goal_init = false;
bool goal_updated = false;
bool map_init = false;
bool map_updated_ = false;
bool goal_reached = false;
std::vector<geometry_msgs::PoseStamped> goal_list_;

std::string global_frame_id = "/map";
std::string jackal_frame_id = "/base_link";
ros::Publisher pub_goal_list;
ros::Publisher pub_global_path;
ros::Publisher pub_current_goal;
ros::Publisher pub_cvMap;

std::mutex mtx_goal_reached;

namespace signal_handle {
    void signal_callback_handler(int signum) {
        std::cout << "\033[34;1m" << "Caught signal "<<  signum << "\033[32;0m" <<std::endl;
        // Terminate program
        exit(signum);
    }
} // namespace signal_handle

void callbackGoal(const geometry_msgs::PoseStamped::ConstPtr &msg){
    ROS_INFO_STREAM("Sub Goal");
    // goal_pt_ = *msg;
    // goal_pt_ = *msg;
    goal_list_.insert(goal_list_.begin(), *msg);
    goal_init = true;
    goal_updated = true;
    return;
};

void callbackMap(const nav_msgs::OccupancyGrid::ConstPtr &msg){
    global_frame_id = msg->header.frame_id;
    // ROS_INFO("Sub Map");
    occmap_ = *msg;
    map_updated_ = true;
    map_init = true;
    return;
};

geometry_msgs::PoseStamped setGoal(double x, double y, double oz, double ow) {
    geometry_msgs::PoseStamped goal;
    goal.header.frame_id = global_frame_id;
    goal.pose.position.x = x;
    goal.pose.position.y = y;
    goal.pose.position.z = 0.0;
    tf::Quaternion q_goal;
    q_goal.setX( 0.0);
    q_goal.setY( 0.0);
    q_goal.setZ( oz);
    q_goal.setW( ow);
    q_goal.normalize();
    goal.pose.orientation.x = q_goal.x();
    goal.pose.orientation.y = q_goal.y();
    goal.pose.orientation.z = q_goal.z();
    goal.pose.orientation.w = q_goal.w();
    return goal;
};

void pubGoalVisualize(const std::vector<geometry_msgs::PoseStamped> &goals){
    pcl::PointCloud<pcl::PointXYZI> goal_cloud;
    goal_cloud.clear(); goal_cloud.reserve(goals.size());
    for (int i = 0; i < goals.size(); i++){
        pcl::PointXYZI pt;
        pt.x = goals[i].pose.position.x;
        pt.y = goals[i].pose.position.y;
        pt.z = 0.0;
        pt.intensity = i;
        goal_cloud.push_back(pt);
    }
    sensor_msgs::PointCloud2 goal_cloud_msg;
    pcl::toROSMsg(goal_cloud, goal_cloud_msg);
    goal_cloud_msg.header.frame_id = global_frame_id;
    goal_cloud_msg.header.stamp = ros::Time::now();
    pub_goal_list.publish(goal_cloud_msg);
    
    return;
};

bool isGoalReached(const geometry_msgs::PoseStamped &_robot_pose, const geometry_msgs::PoseStamped &_goal_pose, double _threshold){
    double dx = _goal_pose.pose.position.x - _robot_pose.pose.position.x;
    double dy = _goal_pose.pose.position.y - _robot_pose.pose.position.y;
    double dist = sqrt(dx*dx + dy*dy);
    if (dist < _threshold){
        return true;
    } else {
        return false;
    }
};

bool flag_goal_reached_ = false;
void updateGoals(){
    ROS_INFO_STREAM("Set the default goals");
    geometry_msgs::PoseStamped goal;

    std::cout << "set goal" << std::endl;
    goal_list_.push_back(setGoal(  2.0, 2.0, 0.5, 0.5));
    goal_list_.push_back(setGoal( -5.0, 1.0, 0.99, 0.1));
    goal_list_.push_back(setGoal( -8.0, -4.0, 0.99, 0.1));
    goal_list_.push_back(setGoal( -2.0, 0.0, 0.0, 1.0));
    goal_list_.push_back(setGoal( 14.0, -1.0, 0.053, 0.998)); // for local minima
    goal_list_.push_back(setGoal( 10.0, 1.0, 0.053, 0.998));
    goal_list_.push_back(setGoal( 13.0, 10.5, 0.053, 0.998));
    goal_list_.push_back(setGoal( 20.0, -4.0, 0.053, 0.998));
    goal_list_.push_back(setGoal( 0.0, 0.0, 1.0, 0.0));    
    goal_init = true;
    goal_reached = false;
    goal_updated = true;
    std::cout << "Default goal_list_.size(): " << goal_list_.size() << std::endl;

    while(ros::ok()){
        
        mtx_goal_reached.lock();
        bool goal_reached = false;
        goal_reached = flag_goal_reached_;
        mtx_goal_reached.unlock();

        if (goal_reached){
            ROS_INFO_STREAM("Goal Reached");
            // goal_list_.pop_front();
            goal_list_.erase(goal_list_.begin());
            goal_reached = false;
            goal_updated = true;
            if (goal_list_.size() == 0){
                ROS_INFO_STREAM("All goals are reached");
                break;
            }

            mtx_goal_reached.lock();
            flag_goal_reached_ = false;
            mtx_goal_reached.unlock();
        }

        if (goal_updated){
            ROS_INFO_STREAM("Update the goal");
            goal_pt_ = goal_list_.front();
            goal_updated = false;
        }
        std::cout << "Goal_list_.size(): " << goal_list_.size() << std::endl;
        std::cout << "Current goal pose: "<< goal_pt_.pose.position.x << ", " << goal_pt_.pose.position.y << std::endl;
        pubGoalVisualize(goal_list_);
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); // 1000ms (you can change the waiting time here)
    }
}


bool planGlobalPath (const nav_msgs::OccupancyGrid &_i_map, const geometry_msgs::PoseStamped _i_robot_pose, const geometry_msgs::PoseStamped _i_goal_pose
                    , nav_msgs::Path &_o_path, geometry_msgs::PoseStamped &_o_current_goal){
    /*
        Function:
            - This function generates the global path (as waypoints) to the goal
        Inputs:       
            - _i_map: occupancy grid map
            - _i_robot_pose: robot pose in the map frame
            - _i_goal_pose: goal pose in the map frame
        Outputs:
            - _o_path: the caculated global path to the goal
            - _o_current_goal: the caculated  next goal point to go
        Return:
            - true if the global path is successfully planned, false otherwise 
    */
    _o_path.header = _i_map.header;
    _o_path.poses.clear();
    _o_path.poses.push_back(_i_robot_pose); // start point = robot pose (idx=0)
    {
        {   // Example code for the conversion between "nav_msgs::OccupancyGrid" and "cv::Mat"
            /*
            * Tips!: If you are non-familiar with "nav_msgs::OccupancyGrid",
            * you can use "OpenCV" with the following conversion functions.
            * Under codes are the example of the conversion between "nav_msgs::OccupancyGrid" and "cv::Mat"
            */
            ee478_utils::OccuMapCv occuMapCv;
            ee478_utils::convtOccmap2CVmap(_i_map, occuMapCv);
            cv::Point curr_pt, goal_pt;
            curr_pt = ee478_utils::getPoseInCvmap(_i_robot_pose.pose, occuMapCv);
            goal_pt = ee478_utils::getPoseInCvmap(_i_goal_pose.pose, occuMapCv);

            // std::cout << "Current pose in cvMap: " << curr_pt << std::endl;
            // std::cout << "Goal pose in cvMap: " << goal_pt << std::endl;
            cv::Mat cvMap = occuMapCv.cvMapForPlan.clone();
            cv::circle(cvMap, curr_pt, 10, cv::Scalar(0, 0, 255), 2);
            cv::circle(cvMap, goal_pt, 10, cv::Scalar(255, 0, 0), 2);
            cv::line(cvMap, curr_pt, goal_pt, cv::Scalar(0, 255, 0), 8);
            cv::flip(cvMap, cvMap, 0); 
            sensor_msgs::Image cvMapMsg;
            cvMapMsg.header.stamp = ros::Time::now();
            ee478_utils::convtMat2SensorImg(cvMap, cvMapMsg, _i_map.header.frame_id);
            pub_cvMap.publish(cvMapMsg);
            // End of the example code
        }
        /*
        * TASK! : 
            - Develope your own global path planner!!!
        * Input : 
            - Use the occupancy grid map (_i_map), robot location (_i_robot_pose), goal position (_i_goal_pose)
        * Output: 
            - Set the global path(_o_path) and the closest edge of global path for local path planner (_o_current_goal)
                (insert your calculated waypoints to '_o_path')
        */
        // YOUR WORK STARTS HERE

    }
    _o_path.poses.push_back(_i_goal_pose);  // end point = goal pose (idx = N-1)
    _o_current_goal = _o_path.poses[1];     // next goal point: the second point of the global path (idx=1)
    _o_current_goal.header = _i_map.header;
    if (_o_path.poses.size() >= 2){
        return true;
    } else {
        return false;
    }
};


void myPlanner(){
    ROS_INFO_STREAM("Turn on Your Own Global Path Planner");
    while(ros::ok()){
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); // 100ms (you can change the waiting time here)
        std::cout << std::endl;

        /////////////////////////////////////////////////
        // 0. Check the initialization of map and goal //
        /////////////////////////////////////////////////
    
        if (!map_init || !goal_init){ // wait for map and goal init
            if (!map_init)
                // ROS_INFO_STREAM("[GPP] Wait for Map init");
            if (!goal_init)
                // ROS_INFO_STREAM("[GPP] Wait for Goal");
            continue;
        }

        static double robot_x, robot_y, robot_yaw;
        try { // get transform from map to base_link: localize the robot
            tf::TransformListener listener_;
            tf::StampedTransform transform;
            listener_.waitForTransform(global_frame_id, jackal_frame_id, ros::Time(0), ros::Duration(10.0));
            listener_.lookupTransform(global_frame_id, jackal_frame_id, ros::Time(0), transform);
            robot_x = transform.getOrigin().x();
            robot_y = transform.getOrigin().y();
            robot_yaw = tf::getYaw(transform.getRotation());

        } catch (tf::TransformException &ex) {
            ROS_ERROR("%s",ex.what());
            continue;
        }
        std::cout << "[Robot] X = " << robot_x << ", Y = " << robot_y << ", Yaw =" << robot_yaw*180.0/M_PI << std::endl;
        std::cout << "[Goal]  X = " << goal_pt_.pose.position.x << ", Y = " << goal_pt_.pose.position.y << ", Yaw = " << tf::getYaw(goal_pt_.pose.orientation)*180.0/M_PI << std::endl;


        ////////////////////////////////////////////
        // 1. Set inputs for global path planning //
        ////////////////////////////////////////////
        nav_msgs::OccupancyGrid current_map;
        
        nav_msgs::Path output_global_path;
        geometry_msgs::PoseStamped output_current_goal;
        geometry_msgs::PoseStamped current_robot_pose, global_goal_pose;

        current_map = occmap_;                          // copy the current grid map
        current_robot_pose.header = current_map.header; // set the robot pose
        current_robot_pose.pose.position.x = robot_x;
        current_robot_pose.pose.position.y = robot_y;
        current_robot_pose.pose.orientation = tf::createQuaternionMsgFromYaw(robot_yaw);
        global_goal_pose = goal_pt_;                    // set the goal pose


        /////////////////////////////////////////
        // 2. Plan the global path to the goal //
        /////////////////////////////////////////
        // TASK: To be implemented in the function "planGlobalPath"
        bool success = planGlobalPath(current_map, current_robot_pose, global_goal_pose,    // input: map, robot pose, goal pose
                                      output_global_path, output_current_goal);             // output: global path, next goal pose
        if (success){
            ROS_INFO_STREAM("[GPP] Update Global Path");

            output_global_path.header.frame_id = global_frame_id;
            output_global_path.header.stamp = ros::Time::now();
            output_current_goal.header.frame_id = global_frame_id;
            output_current_goal.header.stamp = ros::Time::now();

            pub_global_path.publish(output_global_path);
            pub_current_goal.publish(output_current_goal);
        } else {
            ROS_ERROR_STREAM("[GPP] Global Path Planning Failed");
        }
        
        mtx_goal_reached.lock();
        bool goal_reached = isGoalReached(current_robot_pose, global_goal_pose, 0.5);
        flag_goal_reached_ = goal_reached;
        mtx_goal_reached.unlock();
    }
}

int main(int argc, char **argv) {

    ros::init(argc, argv, "ee478_gpp");

    ros::NodeHandle nh;
    ros::NodeHandle nh_private = ros::NodeHandle("~");
    std::cout << "[GPP] Start EE478-Global path planner" << std::endl;

    ros::Subscriber sub_map  = nh.subscribe<nav_msgs::OccupancyGrid>("/map",1,callbackMap);
    ros::Subscriber sub_goal = nh.subscribe<geometry_msgs::PoseStamped>("/goal",1,callbackGoal);

    pub_goal_list           = nh.advertise<sensor_msgs::PointCloud2>("/gpp/goal_list", 1);
    pub_global_path         = nh.advertise<nav_msgs::Path>("/gpp/waypoints_to_goal", 1);
    pub_current_goal        = nh.advertise<geometry_msgs::PoseStamped>("/gpp/current_goal", 1);
    pub_cvMap               = nh.advertise<sensor_msgs::Image>("/gpp/cv_map", 1);


    signal(SIGINT, signal_handle::signal_callback_handler);
    std::thread global_planning{myPlanner};
    std::thread goal_update{updateGoals};
    ros::spin();
    global_planning.join();

    return 0;
}
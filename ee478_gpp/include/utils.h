#ifndef UTILS_H
#define UTILS_H
#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <mutex>
#include <thread>
#include <deque>
#include <cmath>

#include <ros/ros.h>
#include <signal.h>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "opencv2/opencv.hpp"
#include "cv_bridge/cv_bridge.h"
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/conditional_removal.h>
#include <std_msgs/Bool.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

namespace ee478_utils{
    // For the conversion between occupancy grid map and cv image (For openCV familar users) 

    struct OccuMapCv{
        float resolution;
        cv::Mat cvMapForPlan;
        Eigen::MatrixXf m_T_global2gridmap;
    };

    Eigen::MatrixXf geoPoint2Eigen(const geometry_msgs::Point _geoPoint){
        Eigen::MatrixXf result(4,1);
        result << _geoPoint.x, _geoPoint.y, _geoPoint.z, 1;
        return result;
    };

    Eigen::Matrix4f geoPose2Eigen(const geometry_msgs::Pose _geoPose){
        Eigen::Matrix4f result = Eigen::Matrix4f::Identity();
        tf::Quaternion q(_geoPose.orientation.x, _geoPose.orientation.y, _geoPose.orientation.z, _geoPose.orientation.w);
        tf::Matrix3x3 m(q);
        result(0,0) = m[0][0];
        result(0,1) = m[0][1];
        result(0,2) = m[0][2];
        result(1,0) = m[1][0];
        result(1,1) = m[1][1];
        result(1,2) = m[1][2];
        result(2,0) = m[2][0];
        result(2,1) = m[2][1];
        result(2,2) = m[2][2];
        result(3,3) = 1;

        result(0,3) = _geoPose.position.x;
        result(1,3) = _geoPose.position.y;
        result(2,3) = _geoPose.position.z;

        return result;
    };    

    cv::Mat occumap2cvimg(const nav_msgs::OccupancyGrid &_occumap) {
        // unkown = -1 -> 99~149, free : 0:50 ->250:150, occupied :51:100 -> 0:98
        double resolution = _occumap.info.resolution;
        cv::Point origin_cvpt(-_occumap.info.origin.position.x / resolution,
                                -_occumap.info.origin.position.y / resolution);
        cv::Size img_size;
        cv::Mat cvimg = cv::Mat::zeros(_occumap.info.height, _occumap.info.width,CV_8UC1);
        //ROS_INFO("[OCCUGRID 2 CVT CONVERSION] GRID SIZE : %d",occumap.data.size());
        //ROS_INFO("[OCCUGRID 2 CVT CONVERSION] CV ORIGIN : [%d,%d]",origin_cvpt.x,origin_cvpt.y);
        for(int pt = 0;pt < _occumap.data.size();pt++)
        {
            int pt_y = pt / _occumap.info.width;
            int pt_x = pt % _occumap.info.width;
            int value = _occumap.data.at(pt);
            unsigned char img_value;
            if(value == -1) img_value = 120;
            else if (value <= 50) img_value = 250 - 2 * value;
            else if (value >=51) img_value = 200 - 2 * value;
            cvimg.at<unsigned char>(pt_y,pt_x) = img_value;
        }
        return cvimg;
    };

    void convtOccmap2CVmap(const nav_msgs::OccupancyGrid & _occumap, OccuMapCv & _occumap_cv){
        float resolution = _occumap.info.resolution;
        _occumap_cv.resolution = resolution;
        cv::Mat mapIn = occumap2cvimg(_occumap);
        _occumap_cv.cvMapForPlan = mapIn.clone();
        _occumap_cv.m_T_global2gridmap = Eigen::Matrix4f::Identity();
        _occumap_cv.m_T_global2gridmap(0,0) = 1./resolution;
        _occumap_cv.m_T_global2gridmap(1,1) = 1./resolution;
        _occumap_cv.m_T_global2gridmap(0,3) = -_occumap.info.origin.position.x / resolution;
        _occumap_cv.m_T_global2gridmap(1,3) = -_occumap.info.origin.position.y / resolution;
        return;
    };

    cv::Point getPoseInCvmap(const geometry_msgs::Pose & _pose, const OccuMapCv & _occumap_cv){
        Eigen::Matrix4f m_T_global2gridmap = _occumap_cv.m_T_global2gridmap;
        Eigen::Matrix4f m_T_global2pose = geoPose2Eigen(_pose);
        Eigen::Matrix4f m_T_gridmap2pose = m_T_global2gridmap * m_T_global2pose;
        cv::Point poseInCvmap;
        poseInCvmap.x = m_T_gridmap2pose(0,3);
        poseInCvmap.y = m_T_gridmap2pose(1,3);
        return poseInCvmap;
    };

    void convtMat2SensorImg(cv::Mat mat, sensor_msgs::Image& sensorImg, std::string frameID){
        cv_bridge::CvImage bridge;
        mat.copyTo(bridge.image);
        bridge.header.frame_id = frameID;
        bridge.header.stamp = sensorImg.header.stamp;
        if(mat.type() == CV_8UC1)
        {
            bridge.encoding = sensor_msgs::image_encodings::MONO8;
        }
        else if(mat.type() == CV_8UC3)
        {
            bridge.encoding = sensor_msgs::image_encodings::BGR8;
        }
        else if(mat.type() == CV_32FC1)
        {
            bridge.encoding = sensor_msgs::image_encodings::TYPE_32FC1;
        }
        else if(mat.type() == CV_16UC1)
        {
            bridge.encoding = sensor_msgs::image_encodings::TYPE_16UC1;
        }
        else
        {
            std::cout <<"Error : mat type" << std::endl;
        }
        bridge.toImageMsg(sensorImg);
    };
};


#endif
